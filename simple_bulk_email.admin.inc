<?php

/**
 * @file
 * Administrative functions for Simple Bulk Email module.
 */

/**
 * Form builder function, display a list of simplenews categories.
 *
 * @ingroup forms
 */
function simple_bulk_email_settings_form($form, $form_state) {

  $form['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron ( Override )'),
  );
  $form['cron']['simple_bulk_email_settings_markup'] = array(
    '#markup' => t('Fine tune the cron interval to less than one hour. ') .
    t('This will allow you to override the settings located under: System > Cron'),
  );
  $form['cron']['cron_safe_threshold_override'] = array(
    '#type' => 'select',
    '#title' => t('Run cron every'),
    '#default_value' => variable_get('cron_safe_threshold', DRUPAL_CRON_DEFAULT_THRESHOLD),
    '#options' => drupal_map_assoc(
        array(300, 600, 900, 1200, 1500, 1800, 2100, 2400, 2700, 3000, 3300,
          3600, 10800, 21600, 43200, 86400, 604800), 'format_interval'),
    '#description' => t('Cron takes care of running periodic tasks like checking for updates and indexing content for search.'),
  );
  $status = '<p>' . t('Last run: %cron-last ago.', array(
    '%cron-last' => format_interval(REQUEST_TIME - variable_get('cron_last')))) . '</p>';
  $form['status'] = array(
    '#markup' => $status,
  );
  $form['#submit'] = array('simple_bulk_email_settings_form_submit');

  return system_settings_form($form);
}

/**
 * Form submit function.
 */
function simple_bulk_email_settings_form_submit($form, &$form_state) {

  if (!empty($form_state['input']['cron_safe_threshold_override'])) {
    variable_set('cron_safe_threshold', $form_state['input']['cron_safe_threshold_override']);
  }
}
