
DESCRIPTION
-----------

Simple Bulk Email is designed to send email out in bulk and in batches to
hundreds or thousands of recipients. This module creates three blocks.
One lets you send email. Another lets you manage email addresses and
collections of email  addresses. The third lets anonymous and authenticated
users un-subscribe their email addresses.

Features
 1. Send email in bulk at once as an email "blast".
 2. Sends email in batches using cron. User can specify the number of recipients
    to send to per batch.
 3. Can Send email by specific date using a pop-up calendar.
 4. Maintains a backup copy of all email addresses, which is managed by a
    user with administrator role access. Allows user to recover addresses from
    backup.
 5. Allows anonymous and authenticated users to opt-out.
 6. Allows user to upload an attachment.
 7. Manages email templates for quickly composing an email.


REQUIREMENTS
------------

 * Mail engine such as mime mail.


INSTALLATION
------------

 1. CREATE DIRECTORY

    Create a new directory "simple_bulk_email" in the sites/all/modules
    directory and place the entire contents of this simple_bulk_email
    folder in it.

 2. ENABLE THE MODULE

    Enable the module on the Modules admin page.

 3. ACCESS PERMISSION

    Grant the access at the Access control page:
      People > Permissions.

 4. CONFIGURE SIMPLE BULK EMAIL

    Configure Simple Bulk Email on the admin page:
      Configuration > System > Simple Bulk Email.

    Verify cron is adjusted to the correct interval.

 5. ENABLE AND CONFIGURE SIMPLE BULK EMAIL MANAGER BLOCK

    Configure the Simple Bulk Email blocks on the Block configuration page.
    You reach this page from Block admin page (Structure > Blocks).

    Click the 'Configure' link of the appropriate blocks:
        Simple Bulk Email Manager
        Simple Bulk Email Send
        Simple Bulk Email Unsubscribe

 5. CREATE A BASIC PAGE FOR SIMPLE BULK EMAIL UNSUBSCRIBE

    The page must be published and accessible to anonymous users.
    Specify the appropriate path to this page in the
    Simple Bulk Email Unsubscribe block.

 5. (OPTIONAL) CREATE A VIEW FOR SIMPLE BULK EMAIL

    If you want to use Simple Bulk Email as restricted access menu items,
    you can create a view: Structure > Views > Add new view

    Specify the appropriate paths and Accesses under Page settings.
    Then use those same paths in the Simple Bulk Email blocks:
        Simple Bulk Email Manager
        Simple Bulk Email Send



USE - SEE BLOCKS
------------

Each block contains the appropriate instructions for how to use that block.


RELATED MODULES
------------

 * Mime Mail
   Send MIME-encoded emails with embedded images and attachments.
   http://drupal.org/project/mimemail
 * Mailsystem
   Provides a user interface for per-module and site-wide mail_system selection.
   http://drupal.org/project/mailsystem
 * PHPMailer
   Integrates the PHPMailer library for SMTP e-mail delivery.
   http://drupal.org/project/phpmailer
